package com.lolkek.sstest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.lolkek.sstest.screen.searchform.SearchFormFragment

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    if (savedInstanceState == null) {
      supportFragmentManager.beginTransaction().add(R.id.container, SearchFormFragment.newInstance()).commit()
    }
  }
}
