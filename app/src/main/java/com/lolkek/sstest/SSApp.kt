package com.lolkek.sstest

import com.jakewharton.threetenabp.AndroidThreeTen
import com.lolkek.sstest.di.DaggerAppComponent
import dagger.android.support.DaggerApplication
import io.reactivex.plugins.RxJavaPlugins
import timber.log.Timber
import timber.log.Timber.DebugTree

class SSApp : DaggerApplication() {

  override fun onCreate() {
    super.onCreate()
    if (BuildConfig.DEBUG) {
      Timber.plant(DebugTree())
    }
    AndroidThreeTen.init(this)
    RxJavaPlugins.setErrorHandler(Timber::w)
  }

  override fun applicationInjector() = DaggerAppComponent.builder().application(this).build()
}
