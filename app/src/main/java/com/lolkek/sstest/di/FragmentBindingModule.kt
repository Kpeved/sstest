package com.lolkek.sstest.di

import com.lolkek.sstest.screen.results.ResultsFragment
import com.lolkek.sstest.screen.results.di.ResultsFragmentModule
import com.lolkek.sstest.screen.searchform.SearchFormFragment
import com.lolkek.sstest.screen.searchform.di.SearchFormFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {
  @FragmentScope
  @ContributesAndroidInjector(modules = [ResultsFragmentModule::class])
  abstract fun resultsFragment(): ResultsFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [SearchFormFragmentModule::class])
  abstract fun searchFormFragment(): SearchFormFragment

}