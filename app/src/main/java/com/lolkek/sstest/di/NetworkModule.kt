package com.lolkek.sstest.di

import com.lolkek.ssapi.SSApi
import com.lolkek.sstest.Constants
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {
  @Provides
  @Singleton
  fun providesOkHttpClient(logInterceptor: Interceptor) =
      OkHttpClient.Builder()
          .addInterceptor(logInterceptor)
          .readTimeout(60, TimeUnit.SECONDS)
          .connectTimeout(60, TimeUnit.SECONDS)
          .build()

  @Provides
  @Singleton
  fun provideHttpLoggingInterceptor(): Interceptor =
      HttpLoggingInterceptor { message -> Timber.tag("Network").d(message) }
          .apply { level = HttpLoggingInterceptor.Level.BODY }

  @Provides
  @Singleton
  fun provideSSApi(okHttpClient: OkHttpClient): SSApi {
    val retrofit = Retrofit.Builder().client(okHttpClient)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(SSApi.SS_BASE_URL)
        .build()
    return SSApi(retrofit, Constants.API_KEY)
  }
}