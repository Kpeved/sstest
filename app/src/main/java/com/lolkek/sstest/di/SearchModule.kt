package com.lolkek.sstest.di

import com.lolkek.ssapi.SSApi
import com.lolkek.sstest.search.SearchDataRepository
import com.lolkek.sstest.search.SearchParamsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SearchModule {

  @Provides
  @Singleton
  fun providesSearchParamsRepository() = SearchParamsRepository()

  @Provides
  @Singleton
  fun providesSearchDataRepository(ssApi: SSApi) = SearchDataRepository(ssApi)

}