package com.lolkek.sstest.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class SSGlideModule : AppGlideModule()