package com.lolkek.sstest.screen.results

import com.lolkek.sstest.screen.results.viewmodel.SearchDataViewModel
import com.lolkek.sstest.screen.results.viewmodel.SearchParamsViewModel

interface ResultsContract {
  interface Presenter {
    fun subscribe(view: View)
    fun unsubscribe()
  }

  interface View {
    fun bindData(viewModel: SearchDataViewModel)
    fun setSearchParamsData(viewModel: SearchParamsViewModel)
  }
}