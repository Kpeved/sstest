package com.lolkek.sstest.screen.results

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.lolkek.sstest.R
import com.lolkek.sstest.screen.results.view.ResultsRecyclerViewAdapter
import com.lolkek.sstest.screen.results.viewmodel.SearchDataViewModel
import com.lolkek.sstest.screen.results.viewmodel.SearchParamsViewModel
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_results.infoTextView
import kotlinx.android.synthetic.main.fragment_results.messageTextView
import kotlinx.android.synthetic.main.fragment_results.progressBar
import kotlinx.android.synthetic.main.fragment_results.resultsRecyclerView
import kotlinx.android.synthetic.main.fragment_results.toolbar
import java.lang.StringBuilder
import javax.inject.Inject

class ResultsFragment : ResultsContract.View, DaggerFragment() {
  @Inject
  lateinit var presenter: ResultsContract.Presenter

  private lateinit var recyclerViewAdapter: ResultsRecyclerViewAdapter

  override fun setSearchParamsData(viewModel: SearchParamsViewModel) {
    viewModel.apply {
      toolbar.title = createTitleString(places)
      toolbar.subtitle = createSubtitleString(viewModel)
    }
  }

  override fun bindData(viewModel: SearchDataViewModel) {
    progressBar.isVisible = viewModel.inProgress
    messageTextView.isVisible = viewModel.isError
    messageTextView.text = getString(R.string.error_message, viewModel.errorMessage)

    recyclerViewAdapter.bindData(viewModel.itineraries)

    val ticketsCount = viewModel.itineraries.size
    infoTextView.text = resources.getQuantityString(R.plurals.number_of_tickets, ticketsCount, ticketsCount, ticketsCount)
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
    val view = inflater.inflate(R.layout.fragment_results, container, false)
    setHasOptionsMenu(true)
    return view
  }

  override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    inflater.inflate(R.menu.results_menu, menu)
    super.onCreateOptionsMenu(menu, inflater)
  }

  override fun onDestroy() {
    presenter.unsubscribe()
    super.onDestroy()
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    (activity as AppCompatActivity).apply {
      setSupportActionBar(toolbar)
      supportActionBar?.setDisplayShowHomeEnabled(true)
      supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
    presenter.subscribe(this)
    setUpView()
  }

  override fun onDestroyView() {
    presenter.unsubscribe()
    super.onDestroyView()
  }

  private fun setUpView() {
    recyclerViewAdapter = ResultsRecyclerViewAdapter()
    resultsRecyclerView.layoutManager = LinearLayoutManager(context)
    resultsRecyclerView.adapter = recyclerViewAdapter
  }

  private fun createSubtitleString(viewModel: SearchParamsViewModel): String {
    val sb = StringBuilder()
    viewModel.apply {
      sb.append(outboundDate)
      if (inboundDate != null) {
        sb.append(" - $inboundDate")
      }
      sb.append(", $adults ${resources.getQuantityString(R.plurals.adults, adults)}")

      if (children != null && children != 0) {
        sb.append(", $children ${resources.getQuantityString(R.plurals.children, children)}")
      }
      if (infants != null && infants != 0) {
        sb.append(", $infants ${resources.getQuantityString(R.plurals.infants, infants)}")
      }
      sb.append(", $tripClass")
    }
    return sb.toString()
  }

  private fun createTitleString(places: List<String>) = places.joinToString(separator = " - ")

  companion object {
    fun newInstance() = ResultsFragment()
  }
}