package com.lolkek.sstest.screen.results

import android.app.Application
import com.lolkek.ssapi.apimodel.Currency
import com.lolkek.ssapi.model.SearchItinerary
import com.lolkek.ssapi.model.SearchLeg
import com.lolkek.ssapi.model.SearchPricingOption
import com.lolkek.ssapi.model.SearchQuery
import com.lolkek.ssapi.model.SearchResult
import com.lolkek.ssapi.model.SearchStatus.ERROR
import com.lolkek.ssapi.model.SearchStatus.IN_PROGRESS
import com.lolkek.ssapi.params.SearchParams
import com.lolkek.ssapi.params.TripClass
import com.lolkek.sstest.R
import com.lolkek.sstest.screen.results.viewmodel.ItineraryViewModel
import com.lolkek.sstest.screen.results.viewmodel.LegViewModel
import com.lolkek.sstest.screen.results.viewmodel.SearchDataViewModel
import com.lolkek.sstest.screen.results.viewmodel.SearchParamsViewModel
import com.lolkek.sstest.search.SearchDataRepository
import com.lolkek.sstest.search.SearchParamsRepository
import com.lolkek.sstest.utils.DateUtils
import com.lolkek.sstest.utils.DateUtils.DEFAULT_TIME_FORMAT
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

class ResultsInteractor @Inject constructor(
    private val application: Application,
    private val searchDataRepository: SearchDataRepository,
    private val searchParamsRepository: SearchParamsRepository
) {

  fun subscribeToSearchDataUpdates() =
      searchDataRepository.searchDataObservable
          .map { convertToViewModel(it) }

  private fun convertToViewModel(searchResult: SearchResult) =
      SearchDataViewModel(
          searchResult.status == IN_PROGRESS,
          searchResult.status == ERROR,
          searchResult.searchData?.run {
            convertItineraries(itineraries, query)
          } ?: emptyList(),
          searchResult.error?.message
      )

  private fun convertItineraries(itineraries: List<SearchItinerary>, query: SearchQuery): List<ItineraryViewModel> {
    return itineraries.map {
      val cheapestPricing = it.pricingOptions.sortedBy { it.price }.first()
      ItineraryViewModel(
          convertLeg(it.outboundLeg),
          it.inboundLeg?.let { convertLeg(it) },
          generateRating(),
          getPrice(query.currency, cheapestPricing),
          getAgency(cheapestPricing)
      )
    }
  }

  private fun generateRating() = DEFAULT_RATING

  private fun getAgency(pricingOption: SearchPricingOption) =
      pricingOption.agents.first().name

  private fun getPrice(currency: Currency, pricingOption: SearchPricingOption) =
      if (currency.symbolOnLeft) {
        "${currency.symbol}${pricingOption.price}"
      } else {
        "${pricingOption.price} ${currency.symbol}"
      }


  private fun convertLeg(leg: SearchLeg) =
      LegViewModel(
          leg.carriers.first().imageUrl,
          convertTime(leg.departure),
          convertTime(leg.arrival),
          leg.originStation.code,
          leg.destinationStation.code,
          leg.carriers.first().name,
          leg.stops.map { it.code },
          convertDuration(leg.duration)
      )

  private fun convertDuration(duration: Int): String {
    val hours = duration / 60
    val minutes = duration % 60
    return application.getString(R.string.duration, hours, minutes)
  }

  private fun convertTime(time: String) =
      DateUtils.formatFromSearchFormatToLocalDateTime(time)
          .format(DateTimeFormatter.ofPattern(DEFAULT_TIME_FORMAT))

  fun getSearchParamsViewModel() =
      searchParamsRepository.observeSearchParams()
          .map { convertToParamsViewModel(it) }

  private fun convertToParamsViewModel(searchParams: SearchParams): SearchParamsViewModel {
    return SearchParamsViewModel(
        listOf(searchParams.originPlace.removeSuffix("-sky"),
            searchParams.destinationPlace.removeSuffix("-sky")),
        DateUtils.formatFrom_yyyy_MM_dd_To_dd_MMM(searchParams.outboundDate),
        searchParams.inboundDate?.let { DateUtils.formatFrom_yyyy_MM_dd_To_dd_MMM(it) },
        searchParams.adults,
        searchParams.children,
        searchParams.infants,
        searchParams.cabinClass ?: TripClass.ECONOMY
    )
  }

  companion object {
    const val DEFAULT_RATING = 10.0
  }
}