package com.lolkek.sstest.screen.results

import com.lolkek.sstest.di.FragmentScope
import com.lolkek.sstest.screen.results.ResultsContract.View
import com.lolkek.sstest.screen.results.viewmodel.SearchDataViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

@FragmentScope
class ResultsPresenter
@Inject constructor(
    private val resultsInteractor: ResultsInteractor
) : ResultsContract.Presenter {
  private var view: ResultsContract.View? = null
  private var disposables = CompositeDisposable()

  override fun subscribe(view: View) {
    this.view = view
    subscribeToSearchDataUpdates()
    showSearchParamsInfo()
  }

  private fun showSearchParamsInfo() {
    resultsInteractor.getSearchParamsViewModel()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
          view?.setSearchParamsData(it)
        }, Timber::e)
        .addTo(disposables)
  }

  override fun unsubscribe() {
    disposables.dispose()
    this.view = null
  }

  private fun subscribeToSearchDataUpdates() {
    resultsInteractor.subscribeToSearchDataUpdates()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({ view?.bindData(it) }, {
          Timber.e(it)
          view?.bindData(SearchDataViewModel(false, true, emptyList(), it.message))
        })
        .addTo(disposables)
  }
}
