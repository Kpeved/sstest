package com.lolkek.sstest.screen.results.di

import com.lolkek.sstest.screen.results.ResultsContract
import com.lolkek.sstest.screen.results.ResultsPresenter
import dagger.Binds
import dagger.Module

@Module
abstract class ResultsFragmentModule {
  @Binds
  abstract fun presenter(presenter: ResultsPresenter): ResultsContract.Presenter
}