package com.lolkek.sstest.screen.results.view

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lolkek.sstest.R
import com.lolkek.sstest.screen.results.view.ResultsRecyclerViewAdapter.CustomViewHolder
import com.lolkek.sstest.screen.results.viewmodel.ItineraryViewModel
import kotlinx.android.synthetic.main.ticket_item.view.agentTextView
import kotlinx.android.synthetic.main.ticket_item.view.inboundSegmentView
import kotlinx.android.synthetic.main.ticket_item.view.outboundSegmentView
import kotlinx.android.synthetic.main.ticket_item.view.priceTextView
import kotlinx.android.synthetic.main.ticket_item.view.ratingImageView
import kotlinx.android.synthetic.main.ticket_item.view.ratingTextView

class ResultsRecyclerViewAdapter : RecyclerView.Adapter<CustomViewHolder>() {

  private var items: List<ItineraryViewModel> = emptyList()

  fun bindData(newItems: List<ItineraryViewModel>) {
    val oldItems = items

    val diffCallback = object : DiffUtil.Callback() {
      override fun getOldListSize(): Int = oldItems.size
      override fun getNewListSize(): Int = newItems.size
      override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean = oldItems[oldPosition] == newItems[newPosition]
      override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean = oldItems[oldPosition] == newItems[newPosition]
    }
    this.items = newItems
    DiffUtil.calculateDiff(diffCallback).dispatchUpdatesTo(this)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
      CustomViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.ticket_item, parent, false))


  override fun getItemCount() = items.count()

  override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
    holder.bind(items[position])
  }

  class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(itinerary: ItineraryViewModel) {
      itinerary.apply {
        itemView.outboundSegmentView.bindData(outboundLeg)
        if (inboundLeg != null) {
          itemView.inboundSegmentView.bindData(inboundLeg)
        } else {
          itemView.inboundSegmentView.visibility = View.GONE
        }
        itemView.ratingTextView.text = rating.toString()
        itemView.priceTextView.text = price
        itemView.agentTextView.text = itemView.context.getString(R.string.agency_name, agencyName)
        itemView.ratingImageView.setImageResource(getRatingImage(rating))
      }
    }

    private fun getRatingImage(rating: Double) =
        when {
          rating > 0 && rating < 2  -> R.drawable.ic_sentiment_very_dissatisfied_black
          rating >= 2 && rating < 4 -> R.drawable.ic_sentiment_dissatisfied_black
          rating >= 4 && rating < 6 -> R.drawable.ic_sentiment_neutral_black
          rating >= 6 && rating < 8 -> R.drawable.ic_sentiment_satisfied_black
          else                      -> R.drawable.ic_sentiment_very_satisfied_black
        }
  }
}