package com.lolkek.sstest.screen.results.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.View
import com.lolkek.sstest.R
import com.lolkek.sstest.glide.GlideApp
import com.lolkek.sstest.screen.results.viewmodel.LegViewModel
import kotlinx.android.synthetic.main.ticket_segment.view.airlineLogo
import kotlinx.android.synthetic.main.ticket_segment.view.segmentsTextView
import kotlinx.android.synthetic.main.ticket_segment.view.stopOversTextView
import kotlinx.android.synthetic.main.ticket_segment.view.timeTextView
import kotlinx.android.synthetic.main.ticket_segment.view.tripTimeTextView

class TicketSegmentView(context: Context, attrSet: AttributeSet) : ConstraintLayout(context, attrSet) {

  init {
    View.inflate(context, R.layout.ticket_segment, this)
  }

  fun bindData(leg: LegViewModel) {
    timeTextView.text = context.getString(R.string.time, leg.departureTime, leg.arrivalTime)
    stopOversTextView.text = getStopOversText(leg.stopOvers)
    segmentsTextView.text = getSegmentsText(leg.departureAirport, leg.arrivalAirport, leg.carrier)
    tripTimeTextView.text = leg.duration
    GlideApp.with(context).load(leg.logoUrl).into(airlineLogo)
  }

  private fun getSegmentsText(departure: String, arrival: String, carrier: String) =
      context.getString(R.string.airports_description, departure, arrival, carrier)


  private fun getStopOversText(stopOvers: List<String>) =
      if (stopOvers.isEmpty()) context.getString(R.string.direct) else stopOvers.joinToString()
}