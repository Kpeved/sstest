package com.lolkek.sstest.screen.results.viewmodel

data class ItineraryViewModel(
    val outboundLeg: LegViewModel,
    val inboundLeg: LegViewModel?,
    val rating: Double,
    val price: String,
    val agencyName: String
)