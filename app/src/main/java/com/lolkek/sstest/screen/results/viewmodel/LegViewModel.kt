package com.lolkek.sstest.screen.results.viewmodel

data class LegViewModel(
    val logoUrl: String,
    val departureTime: String,
    val arrivalTime: String,
    val departureAirport: String,
    val arrivalAirport: String,
    val carrier: String,
    val stopOvers: List<String>,
    val duration: String
)