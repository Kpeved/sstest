package com.lolkek.sstest.screen.results.viewmodel

data class SearchDataViewModel(
    val inProgress: Boolean,
    val isError: Boolean,
    val itineraries: List<ItineraryViewModel>,
    val errorMessage: String? = null
)