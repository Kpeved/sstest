package com.lolkek.sstest.screen.results.viewmodel

data class SearchParamsViewModel(
    val places : List<String>,
    val outboundDate: String,
    val inboundDate: String? = null,
    val adults: Int,
    val children: Int? = null,
    val infants: Int? = null,
    val tripClass: String)