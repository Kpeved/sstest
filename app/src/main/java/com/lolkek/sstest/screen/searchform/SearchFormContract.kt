package com.lolkek.sstest.screen.searchform

interface SearchFormContract {
  interface Presenter {
    fun subscribe(view: View)
    fun unsubscribe()
    fun startSearch()
  }

  interface View {
    fun openResultsFragment()
  }
}