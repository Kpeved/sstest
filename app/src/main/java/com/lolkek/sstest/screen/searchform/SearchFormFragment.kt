package com.lolkek.sstest.screen.searchform

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lolkek.sstest.R
import com.lolkek.sstest.screen.results.ResultsFragment
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_searchform.startSearchButton
import javax.inject.Inject

class SearchFormFragment : SearchFormContract.View, DaggerFragment() {
  @Inject
  lateinit var presenter: SearchFormContract.Presenter

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
      inflater.inflate(R.layout.fragment_searchform, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    presenter.subscribe(this)
    startSearchButton.setOnClickListener {
      presenter.startSearch()
    }
  }

  override fun onDestroyView() {
    presenter.unsubscribe()
    super.onDestroyView()
  }

  override fun openResultsFragment() {
    activity?.apply {
      supportFragmentManager
          .beginTransaction()
          .add(R.id.container, ResultsFragment.newInstance())
          .addToBackStack(null)
          .commit()
    }
  }

  companion object {
    fun newInstance() = SearchFormFragment()
  }
}