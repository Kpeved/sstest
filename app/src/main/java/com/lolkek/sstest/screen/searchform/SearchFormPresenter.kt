package com.lolkek.sstest.screen.searchform

import com.lolkek.sstest.screen.searchform.SearchFormContract.View
import com.lolkek.sstest.search.SearchDataRepository
import com.lolkek.sstest.search.SearchParamsRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class SearchFormPresenter
@Inject constructor(
    private val searchDataRepository: SearchDataRepository,
    private val searchParamsRepository: SearchParamsRepository)
  : SearchFormContract.Presenter {

  private var view: SearchFormContract.View? = null
  private var disposables = CompositeDisposable()

  override fun subscribe(view: View) {
    this.view = view
  }

  override fun unsubscribe() {
    disposables.dispose()
    this.view = null
  }

  override fun startSearch() {
    searchParamsRepository.observeSearchParams()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
          searchDataRepository.startSearch(it)
          view?.openResultsFragment()
        }, Timber::e)
        .addTo(disposables)
  }
}