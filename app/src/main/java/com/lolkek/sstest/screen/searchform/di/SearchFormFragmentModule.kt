package com.lolkek.sstest.screen.searchform.di

import com.lolkek.sstest.screen.searchform.SearchFormContract
import com.lolkek.sstest.screen.searchform.SearchFormPresenter
import dagger.Binds
import dagger.Module

@Module
abstract class SearchFormFragmentModule {
  @Binds
  abstract fun presenter(presenter: SearchFormPresenter): SearchFormContract.Presenter
}