package com.lolkek.sstest.search

import com.jakewharton.rxrelay2.BehaviorRelay
import com.lolkek.ssapi.SSApi
import com.lolkek.ssapi.model.SearchResult
import com.lolkek.ssapi.model.SearchStatus.ERROR
import com.lolkek.ssapi.params.SearchParams
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class SearchDataRepository(private val ssApi: SSApi) {

  private var disposable: Disposable? = null
  private val searchDataStream = BehaviorRelay.create<SearchResult>()
  val searchDataObservable: Observable<SearchResult> = searchDataStream


  fun startSearch(searchParams: SearchParams) {
    disposable?.dispose()
    disposable = ssApi.startSearch(searchParams)
        .subscribeOn(Schedulers.io())
        .subscribe({ searchDataStream.accept(it) }, {
          Timber.e(it)
          searchDataStream.accept(SearchResult(ERROR, error = it))
        })

  }
}