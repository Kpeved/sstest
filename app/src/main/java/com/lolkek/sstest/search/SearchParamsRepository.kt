package com.lolkek.sstest.search

import com.lolkek.ssapi.params.SearchParams
import com.lolkek.sstest.utils.DateUtils
import io.reactivex.Single
import org.threeten.bp.DayOfWeek.MONDAY
import org.threeten.bp.LocalDate
import org.threeten.bp.temporal.TemporalAdjusters

class SearchParamsRepository {

  fun observeSearchParams() = Single.fromCallable {
    loadSearchParams()
  }

  private fun loadSearchParams(): SearchParams {
    val defaultOutboundDate = LocalDate.now().with(TemporalAdjusters.next(MONDAY))
    val defaultInboundDate = defaultOutboundDate.plusDays(1)
    return SearchParams(
        country = "UK",
        currency = "GBP",
        locale = "en_US",
        originPlace = "LOND-sky",
        destinationPlace = "EDI-sky",
        outboundDate = DateUtils.formatToDefaultSearchData(defaultOutboundDate),
        inboundDate = DateUtils.formatToDefaultSearchData(defaultInboundDate),
        adults = 1)
  }
}