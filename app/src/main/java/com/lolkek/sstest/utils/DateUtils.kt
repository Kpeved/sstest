package com.lolkek.sstest.utils

import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

object DateUtils {
  fun formatToDefaultSearchData(date: LocalDate) = date.format(DateTimeFormatter.ofPattern(DEFAULT_DATE_SERVER_FORMAT))

  fun formatFromSearchFormatToLocalDateTime(serverFormattedTime: String) =
      LocalDateTime.parse(serverFormattedTime, DateTimeFormatter.ofPattern(DEFAULT_DATE_TIME_SERVER_FORMAT))

  fun formatFrom_yyyy_MM_dd_To_dd_MMM(serverFormattedTime: String) =
      LocalDate.parse(serverFormattedTime, DateTimeFormatter.ofPattern(DEFAULT_DATE_SERVER_FORMAT))
          .format(DateTimeFormatter.ofPattern(DATE_FORMAT_dd_MMM))

  const val DEFAULT_DATE_SERVER_FORMAT = "yyyy-MM-dd"
  const val DEFAULT_DATE_TIME_SERVER_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
  const val DEFAULT_TIME_FORMAT = "HH:mm"
  const val DATE_FORMAT_dd_MMM = "dd MMM"
}