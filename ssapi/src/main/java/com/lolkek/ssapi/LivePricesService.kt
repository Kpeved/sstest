package com.lolkek.ssapi

import com.lolkek.ssapi.apimodel.LoadedData
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query
import retrofit2.http.Url

interface LivePricesService {

  @FormUrlEncoded
  @POST("pricing/v1.0")
  fun createSession(
      @Field("apiKey") apiKey: String,
      @Field("country") country: String,
      @Field("currency") currency: String,
      @Field("locale") locale: String,
      @Field("originPlace") originPlace: String,
      @Field("destinationPlace") destinationPlace: String,
      @Field("outboundDate") outboundDate: String,
      @Field("inboundDate") inboundDate: String? = null,
      @Field("cabinClass") cabinClass: String? = null,
      @Field("adults") adults: Int,
      @Field("children") children: Int? = null,
      @Field("infants") infants: Int? = null,
      @Field("includeCarriers") includeCarriers: List<String>? = null,
      @Field("excludeCarriers") excludeCarriers: List<String>? = null
  ): Single<Response<Void>>

  @GET
  @Headers("Accept: application/json")
  fun getData(
      @Url locationUrl: String,
      @Query("apiKey") apiKey: String): Call<LoadedData>

  companion object {
    const val LOCATION_HEADER = "Location"
  }
}