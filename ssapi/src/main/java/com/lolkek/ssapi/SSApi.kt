package com.lolkek.ssapi

import com.lolkek.ssapi.converter.SearchDataConverter
import com.lolkek.ssapi.model.SearchResult
import com.lolkek.ssapi.model.SearchStatus.IN_PROGRESS
import com.lolkek.ssapi.params.SearchParams
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import retrofit2.HttpException
import retrofit2.Retrofit

class SSApi(retrofit: Retrofit, private val apiKey: String) {
  private val livePricesService = retrofit.create(LivePricesService::class.java)

  fun startSearch(searchParams: SearchParams) = createSession(searchParams)
      .doOnSuccess { if (!it.isSuccessful) throw HttpException(it) }
      .map { it.headers().get(LivePricesService.LOCATION_HEADER) }
      .flatMapPublisher { retreivingSearchData(it) }
      .startWith(SearchResult(IN_PROGRESS))

  private fun retreivingSearchData(locationUrl: String) =
      Flowable.create(SearchDataFlowable(livePricesService, locationUrl, apiKey), BackpressureStrategy.BUFFER)
          .map { SearchDataConverter.convertToSearchResult(it) }

  private fun createSession(searchParams: SearchParams) = searchParams.run {
    livePricesService.createSession(
        apiKey = apiKey,
        country = country,
        currency = currency,
        locale = locale,
        originPlace = originPlace,
        destinationPlace = destinationPlace,
        outboundDate = outboundDate,
        inboundDate = inboundDate,
        cabinClass = cabinClass,
        adults = adults,
        children = children,
        infants = infants,
        includeCarriers = includeCarriers,
        excludeCarriers = excludeCarriers
    )
  }

  companion object {
    const val SS_BASE_URL = "http://partners.api.skyscanner.net/apiservices/"
  }
}