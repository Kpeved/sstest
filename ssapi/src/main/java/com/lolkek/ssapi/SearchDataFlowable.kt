package com.lolkek.ssapi

import com.lolkek.ssapi.apimodel.LoadedData
import com.lolkek.ssapi.apimodel.LoadedData.Companion.STATUS_UPDATES_COMPLETE
import io.reactivex.FlowableEmitter
import io.reactivex.FlowableOnSubscribe
import retrofit2.HttpException

class SearchDataFlowable(
    private val livePricesService: LivePricesService,
    private val locationUrl: String,
    private val apiKey: String) : FlowableOnSubscribe<LoadedData> {

  override fun subscribe(emitter: FlowableEmitter<LoadedData>) {
    var searchCompleted = false
    try {
      do {
        try {
          Thread.sleep(INTERVAL_TIME)
        } catch (e: InterruptedException) {
          if (!emitter.isCancelled) emitter.onError(Throwable("Thread is interrupted"))
          return
        }
        val response = livePricesService.getData(locationUrl, apiKey).execute()

        if (!response.isSuccessful) {
          if (!emitter.isCancelled) emitter.onError(HttpException(response))
        }

        response.body()?.apply {
          if (status == STATUS_UPDATES_COMPLETE) {
            emitter.onNext(this)
            searchCompleted = true
          } else {
            emitter.onNext(this)
          }
        }
      } while (!searchCompleted)
      emitter.onComplete()
    } catch (e: Exception) {
      if (!emitter.isCancelled) emitter.onError(e)
    }
  }

  companion object {
    private const val INTERVAL_TIME = 3000L
  }
}