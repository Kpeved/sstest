package com.lolkek.ssapi.apimodel

import com.google.gson.annotations.SerializedName

data class Itinerary(
    @SerializedName("OutboundLegId") val outboundLegId: String,
    @SerializedName("InboundLegId") val inboundLegId: String,
    @SerializedName("PricingOptions") val pricingOptions: List<PricingOption>,
    @SerializedName("BookingDetailsLink") val bookingDetailsLink: BookingDetailsLink
)