package com.lolkek.ssapi.apimodel

import com.google.gson.annotations.SerializedName

data class LoadedData(
    @SerializedName("SessionKey") val sessionKey: String,
    @SerializedName("Query") val query: Query,
    @SerializedName("Status") val status: String,
    @SerializedName("Itineraries") val itineraries: List<Itinerary>,
    @SerializedName("Legs") val legs: List<Leg>,
    @SerializedName("Segments") val segments: List<Segment>,
    @SerializedName("Carriers") val carriers: List<Carrier>,
    @SerializedName("Agents") val agents: List<Agent>,
    @SerializedName("Places") val places: List<Place>,
    @SerializedName("Currencies") val currencies: List<Currency>
){
  companion object {
    const val STATUS_UPDATES_COMPLETE = "UpdatesComplete"
  }
}