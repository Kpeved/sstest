package com.lolkek.ssapi.apimodel

import com.google.gson.annotations.SerializedName

data class PricingOption(
    @SerializedName("Agents") val agents: List<Int>,
    @SerializedName("QuoteAgeInMinutes") val quoteAgeInMinutes: Int,
    @SerializedName("Price") val price: Double,
    @SerializedName("DeeplinkUrl") val deeplinkUrl: String
)