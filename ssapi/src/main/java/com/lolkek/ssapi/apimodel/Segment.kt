package com.lolkek.ssapi.apimodel

import com.google.gson.annotations.SerializedName

data class Segment(
    @SerializedName("Id") val id: Int,
    @SerializedName("OriginStation") val originStation: Int,
    @SerializedName("DestinationStation") val destinationStation: Int,
    @SerializedName("DepartureDateTime") val departureDateTime: String,
    @SerializedName("ArrivalDateTime") val arrivalDateTime: String,
    @SerializedName("Carrier") val carrier: Int,
    @SerializedName("OperatingCarrier") val operatingCarrier: Int,
    @SerializedName("Duration") val duration: Int,
    @SerializedName("FlightNumber") val flightNumber: String,
    @SerializedName("JourneyMode") val journeyMode: String,
    @SerializedName("Directionality") val directionality: String
)