package com.lolkek.ssapi.converter

import com.lolkek.ssapi.apimodel.Agent
import com.lolkek.ssapi.apimodel.Carrier
import com.lolkek.ssapi.apimodel.Currency
import com.lolkek.ssapi.apimodel.Itinerary
import com.lolkek.ssapi.apimodel.Leg
import com.lolkek.ssapi.apimodel.LoadedData
import com.lolkek.ssapi.apimodel.Place
import com.lolkek.ssapi.apimodel.PricingOption
import com.lolkek.ssapi.apimodel.Query
import com.lolkek.ssapi.apimodel.Segment
import com.lolkek.ssapi.model.SearchData
import com.lolkek.ssapi.model.SearchItinerary
import com.lolkek.ssapi.model.SearchLeg
import com.lolkek.ssapi.model.SearchPricingOption
import com.lolkek.ssapi.model.SearchQuery
import com.lolkek.ssapi.model.SearchResult
import com.lolkek.ssapi.model.SearchStatus
import com.lolkek.ssapi.model.SearchStatus.IN_PROGRESS

object SearchDataConverter {
  fun convertToSearchResult(loadedData: LoadedData): SearchResult {

    val segmentsMap = loadedData.segments.map { it.id to it }.toMap()
    val placesMap = loadedData.places.map { it.id to it }.toMap()
    val carriersMap = loadedData.carriers.map { it.id to it }.toMap()
    val agentsMap = loadedData.agents.map { it.id to it }.toMap()
    val currenciesMap = loadedData.currencies.map { it.code to it }.toMap()

    val convertedLegsMap = convertLegsMap(loadedData.legs, segmentsMap, placesMap, carriersMap).map { it.id to it }.toMap()
    val convertedItineraries = convertItineraries(loadedData.itineraries, convertedLegsMap, agentsMap)

    return SearchResult(
        convertSearchStatus(loadedData.status),
        SearchData(
            loadedData.sessionKey,
            convertQuery(loadedData.query, currenciesMap),
            loadedData.status,
            convertedItineraries,
            convertedLegsMap,
            segmentsMap,
            carriersMap,
            agentsMap,
            placesMap,
            loadedData.currencies))
  }

  private fun convertQuery(query: Query, currenciesMap: Map<String, Currency>) =
      query.run {
        SearchQuery(
            country,
            checkNotNull(currenciesMap[currency]),
            locale,
            adults,
            children,
            infants,
            originPlace,
            destinationPlace,
            outboundDate,
            inboundDate,
            locationSchema,
            cabinClass,
            groupPricing
        )
      }


  private fun convertSearchStatus(status: String) =
      if (status == LoadedData.STATUS_UPDATES_COMPLETE) SearchStatus.COMPLETED else IN_PROGRESS

  private fun convertItineraries(
      itineraries: List<Itinerary>,
      convertedLegsMap: Map<String, SearchLeg>,
      agentsMap: Map<Int, Agent>) =
      itineraries.map {
        SearchItinerary(
            checkNotNull(convertedLegsMap[it.outboundLegId]),
            convertedLegsMap[it.inboundLegId],
            convertPricingOptions(it.pricingOptions, agentsMap),
            it.bookingDetailsLink)
      }

  private fun convertPricingOptions(pricingOptions: List<PricingOption>, agentsMap: Map<Int, Agent>) =
      pricingOptions.map {
        SearchPricingOption(
            it.agents.map { checkNotNull(agentsMap[it]) },
            it.quoteAgeInMinutes,
            it.price,
            it.deeplinkUrl)
      }

  private fun convertLegsMap(legs: List<Leg>,
      segmentsMap: Map<Int, Segment>,
      placesMap: Map<Int, Place>,
      carriersMap: Map<Int, Carrier>) = legs.map {
    SearchLeg(
        it.id,
        it.segmentIds.map { checkNotNull(segmentsMap[it]) },
        checkNotNull(placesMap[it.originStation]),
        checkNotNull(placesMap[it.destinationStation]),
        it.departure,
        it.arrival,
        it.duration,
        it.journeyMode,
        it.stops.map { checkNotNull(placesMap[it]) },
        it.carriers.map { checkNotNull(carriersMap[it]) },
        it.operatingCarriers.map { checkNotNull(carriersMap[it]) },
        it.directionality,
        it.flightNumbers
    )
  }
}