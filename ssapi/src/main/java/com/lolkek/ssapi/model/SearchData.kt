package com.lolkek.ssapi.model

import com.lolkek.ssapi.apimodel.Agent
import com.lolkek.ssapi.apimodel.Carrier
import com.lolkek.ssapi.apimodel.Currency
import com.lolkek.ssapi.apimodel.Place
import com.lolkek.ssapi.apimodel.Query
import com.lolkek.ssapi.apimodel.Segment

class SearchData(
    val sessionKey: String,
    val query: SearchQuery,
    val status: String,
    val itineraries: List<SearchItinerary>,
    val legs: Map<String, SearchLeg>,
    val segments: Map<Int, Segment>,
    val carriers: Map<Int, Carrier>,
    val agents: Map<Int, Agent>,
    val places: Map<Int, Place>,
    val currencies: List<Currency>
)