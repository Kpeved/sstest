package com.lolkek.ssapi.model

import com.lolkek.ssapi.apimodel.BookingDetailsLink

data class SearchItinerary(
    val outboundLeg: SearchLeg,
    val inboundLeg: SearchLeg?,
    val pricingOptions: List<SearchPricingOption>,
    val bookingDetailsLink: BookingDetailsLink
)