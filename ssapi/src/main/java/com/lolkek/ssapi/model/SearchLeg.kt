package com.lolkek.ssapi.model

import com.lolkek.ssapi.apimodel.Carrier
import com.lolkek.ssapi.apimodel.FlightNumber
import com.lolkek.ssapi.apimodel.Place
import com.lolkek.ssapi.apimodel.Segment

data class SearchLeg(
    val id: String,
    val segments: List<Segment>,
    val originStation: Place,
    val destinationStation: Place,
    val departure: String,
    val arrival: String,
    val duration: Int,
    val journeyMode: String,
    val stops: List<Place>,
    val carriers: List<Carrier>,
    val operatingCarriers: List<Carrier>,
    val directionality: String,
    val flightNumbers: List<FlightNumber>
)