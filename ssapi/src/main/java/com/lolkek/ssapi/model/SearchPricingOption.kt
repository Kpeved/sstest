package com.lolkek.ssapi.model

import com.lolkek.ssapi.apimodel.Agent

data class SearchPricingOption(
     val agents: List<Agent>,
     val quoteAgeInMinutes: Int,
     val price: Double,
     val deeplinkUrl: String
)