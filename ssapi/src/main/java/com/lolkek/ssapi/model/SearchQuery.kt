package com.lolkek.ssapi.model

import com.lolkek.ssapi.apimodel.Currency

data class SearchQuery(
    val country: String,
    val currency: Currency,
    val locale: String,
    val adults: Int,
    val children: Int,
    val infants: Int,
    val originPlace: String,
    val destinationPlace: String,
    val outboundDate: String,
    val inboundDate: String,
    val locationSchema: String,
    val cabinClass: String,
    val groupPricing: Boolean
)