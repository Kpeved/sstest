package com.lolkek.ssapi.model


data class SearchResult(val status: SearchStatus, val searchData: SearchData? = null, val error: Throwable? = null)