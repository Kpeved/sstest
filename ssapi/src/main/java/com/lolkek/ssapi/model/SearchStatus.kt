package com.lolkek.ssapi.model

enum class SearchStatus {
  IN_PROGRESS,
  COMPLETED,
  ERROR
}