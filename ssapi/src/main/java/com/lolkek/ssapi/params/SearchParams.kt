package com.lolkek.ssapi.params

data class SearchParams(
    val country: String,
    val currency: String,
    val locale: String,
    val originPlace: String,
    val destinationPlace: String,
    val outboundDate: String,
    val inboundDate: String? = null,
    val cabinClass: String? = null,
    val adults: Int,
    val children: Int? = null,
    val infants: Int? = null,
    val includeCarriers: List<String>? = null,
    val excludeCarriers: List<String>? = null)