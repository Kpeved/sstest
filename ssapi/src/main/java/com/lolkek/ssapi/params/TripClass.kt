package com.lolkek.ssapi.params

object TripClass {
  const val ECONOMY = "economy"
  const val BUSINESS = "business"
}